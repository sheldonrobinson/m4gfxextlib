// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class libgvdb : ModuleRules
{
	public libgvdb(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;
		
		
		var sIncPath = Path.Combine(ModuleDirectory,"include");
		PublicIncludePaths.AddRange(
			new string[] {
				sIncPath,
				// ... add public include paths required here ...
			}
			);
			
		bool bIsDebugBuildType = false;
		switch (Target.Configuration)
		{
				case UnrealTargetConfiguration.DebugGame:
				case UnrealTargetConfiguration.Debug:
				case UnrealTargetConfiguration.Development:
					bIsDebugBuildType = true;
					break;

				default:
					bIsDebugBuildType = false;
					break;
		}
		
		switch(Target.Platform) {
			case UnrealTargetPlatform.Win64 :
						// Add the import library
						PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "Win64", (bIsDebugBuildType)?"Debug":"Release"));
						PublicAdditionalLibraries.Add("libgvdb.lib");

						// Delay-load the DLL, so we can load it from the right place first
						PublicDelayLoadDLLs.Add("libgvdb.dll");
						break;
			case UnrealTargetPlatform.Linux : 
						// Add the import library
						PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "Linux", (bIsDebugBuildType)?"Debug":"Release"));
						PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Linux", (bIsDebugBuildType)?"Debug":"Release", "libgvdb.so"));
						PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Linux", (bIsDebugBuildType)?"Debug":"Release", "libgvdb.a"));
						break;
			case UnrealTargetPlatform.Android :
						break;
			case UnrealTargetPlatform.Mac :
						PublicDelayLoadDLLs.Add(Path.Combine(ModuleDirectory, "Mac", (bIsDebugBuildType)?"Debug":"Release", "libgvdb.dylib"));
						break;
			case UnrealTargetPlatform.IOS :
						break;
		}
	}
}
