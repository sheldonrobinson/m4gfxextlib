# Platform M extensions for Graphics Engines

This is the project page for **M for Graphics Engines Extensions Toolkit** on [GitLab](https://gitlab.com/sheldonrobinson/m4gfxextlib).
**M4Gfx.X** is being created for Unreal Engine 4.  The purpose is to add advance features to Unreal Engine 4 that fully leverage [Platform M](https:www.augertron.com)..
The features being developed are
1. **M4Gfx.Cloud** - a plugin for cloud gaming using hybrid approach combining both video streaming and cloud paging.  This is based on request found in
 [UE4 Answerhub](https://answers.unrealengine.com/questions/909628/scene-partitioning-enhancement-for-cloud-gaming-in.html) 
2. **M4Gfx.Accel** - a plugin that allows UE4 to  utilized FPGA Accelerators for AI, Inverse Kinematics, Game Physics, etc.

These features are being developed as plugins that can be loaded into existing games without code changes or recompilation.

# M4Gfx.SIS

System Information Summary for the application host.  See [hosted on github, M4GfxSIS](https://github.com/sheldonrobinson/M4GfxSIS)

# M4Gfx.Cloud

The **M4Gfx.Cloud** is comprised of two(2) components:
1. **M4GfxServer** - is a hybrid gaming server that runs on a remote host. It may provide a video stream, cloud paging and/or file stream based on client capabilities 
that allows the  best gaming-on-demand user experience.
2. **M4GfxTerminal** - is the UE4 Runtime with M4Gfx.Cloud plugin enabled.  UE4Game acts as a thin client to remote host with ability run the game stream and/or play the pixel stream.

### M4GfxServer & M4GfxTerminal
These are based on the 
- [Pixel Streaming Demo](https://docs.unrealengine.com/en-US/Resources/Showcases/PixelStreamingShowcase/index.html) which demostrates how UE4 can acts as a cloud gaming server
- [Composure Framework](https://docs.unrealengine.com/en-US/Engine/Composure/index.html) which shows howto composit different asset to create a final scene
- RemoteSession and BackChannel plugins which enable replay of remote inputs and ingesting of remote instructions

![M4Gfx.Cloud](/doc/images/R3Plugin-hierarchical-diagram.jpg "M for Graphics Engines Cloud Gaming Extension")

# M4Gfx.Accel
Coming Soon
