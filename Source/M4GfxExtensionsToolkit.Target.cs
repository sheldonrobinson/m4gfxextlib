// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class M4GfxExtensionsToolkitTarget : TargetRules
{
	public M4GfxExtensionsToolkitTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Client;

		ExtraModuleNames.AddRange( new string[] { "M4GfxExtensionsToolkit"} );
		

	}
}