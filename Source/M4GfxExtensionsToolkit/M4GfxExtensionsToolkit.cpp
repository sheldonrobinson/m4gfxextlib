// Copyright 2019 Sheldon Robinson, Inc. All Rights Reserved.

#include "M4GfxExtensionsToolkit.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, M4GfxExtensionsToolkit, "M4GfxExtTk" );
