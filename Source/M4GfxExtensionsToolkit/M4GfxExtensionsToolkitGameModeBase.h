// Copyright 2019 Sheldon Robinson, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "M4GfxExtensionsToolkitGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class M4GFXEXTENSIONSTOOLKIT_API AM4GfxExtensionsToolkitGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	
	
	
};
