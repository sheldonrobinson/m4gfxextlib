// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class M4GfxExtensionsToolkitEditorTarget : TargetRules
{
	public M4GfxExtensionsToolkitEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "M4GfxExtensionsToolkitEditor", "M4GfxExtensionsToolkit", "M4GfxExtensionsToolkitServer" } );
	}
}
