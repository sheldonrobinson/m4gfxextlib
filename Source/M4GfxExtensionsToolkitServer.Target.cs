// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class M4GfxExtensionsToolkitServerTarget : TargetRules
{
	public M4GfxExtensionsToolkitServerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		
		bUsesSlate = false;

		ExtraModuleNames.AddRange( new string[] { "M4GfxExtensionsToolkit", "M4GfxExtensionsToolkitServer"} );
	}
}
