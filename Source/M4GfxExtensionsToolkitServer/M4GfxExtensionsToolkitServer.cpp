// Copyright 2019 Sheldon Robinson, Inc. All Rights Reserved.

#include "M4GfxExtensionsToolkitServer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE ( FDefaultGameModuleImpl, M4GfxExtensionsToolkitServer);
