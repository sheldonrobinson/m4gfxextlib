// Copyright 2019 Sheldon Robinson, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "M4GfxExtensionsToolkitServerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class M4GFXEXTENSIONSTOOLKITSERVER_API AM4GfxExtensionsToolkitServerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	
	
	
};
